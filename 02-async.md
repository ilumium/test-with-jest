# Testing Async Code

## Promises

```javascript
function fetchData() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("bread with peanut butter");
    }, 1000);
  });
}

test("the data contains peanut butter", () => {
  return fetchData().then(data => {
    try {
      expect(data).toMatch(/peanut butter/);
    } catch (error) {
      // If the promise is rejected, Jest will automatically fail the test.
      expect(error).toMatch("error");
    }
  });
});
```

## Async/Await

```javascript
function fetchData() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("bread with peanut butter");
    }, 1000);
  });
}

test("the data contains peanut butter", async () => {
  const data = await fetchData();
  try {
    expect(data).toMatch(/peanut butter/);
  } catch (error) {
    expect(error).toMatch("error");
  }
});
```

## Callbacks

```javascript
function fetchData(callback) {
  setTimeout(() => {
    callback("bread with peanut butter");
  }, 1000);
}

test("the data contains peanut butter", (done) => {
  function callback(data) {
    try {
      expect(data).toMatch(/peanut butter/);
      done();
    } catch (error) {
      done.fail(error);
    }
  }

  fetchData(callback);
});
```

## Promises with .resolves/.rejects

```javascript
function fetchData() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("bread with peanut butter");
    }, 1000);
  });
}

test("the data contains peanut butter", () => {
  return expect(fetchData()).resolves.toMatch(/peanut butter/);
});
```

```javascript
function fetchData() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      reject("error");
    }, 1000);
  });
}

test("the data contains peanut butter", () => {
  return expect(fetchData()).rejects.toMatch("error");
});
```
