const { Vector } = require("./vectors");

describe("Vector creation", () => {
  test("throws an error if no arguments are provided", () => {
    expect(() => new Vector()).toThrow("No arguments provided");
  });

  test("throws an error if any argument is not a number", () => {
    expect(() => new Vector(1, 2, "3")).toThrow(
      "All arguments must be numbers",
    );
  });

  test("creates a new Vector object", () => {
    const v = new Vector(1, 2, 3);
    expect(v).toBeInstanceOf(Vector);
  });

  test("creates a new Vector object", () => {
    const v = new Vector(1, 2, 3);
    expect(v.vector).toEqual([1, 2, 3]);
  });

  test("creates a new Vector object with float numbers", () => {
    const v = new Vector(1.1, 2.2, 3.3);
    expect(v.vector).toEqual([1.1, 2.2, 3.3]);
  });
});

describe("Vector addition", () => {
  test("throws an error if the vectors have different lengths", () => {
    const v1 = new Vector(1, 2, 3);
    const v2 = new Vector(4, 5);
    expect(() => v1.add(v2)).toThrow("Vectors must have the same length");
  });

  test("throws an error if the argument is not a Vector", () => {
    const v1 = new Vector(1, 2, 3);
    expect(() => v1.add(1)).toThrow("Argument must be a Vector");
  });

  test("returns a new Vector object", () => {
    const v1 = new Vector(1, 2, 3);
    const v2 = new Vector(4, 5, 6);
    const v3 = v1.add(v2);
    expect(v3).toBeInstanceOf(Vector);
  });

  test("returns a new Vector object with the sum of the vectors", () => {
    const v1 = new Vector(1, 2, 3);
    const v2 = new Vector(4, 5, 6);
    const v3 = v1.add(v2);
    expect(v3.vector).toEqual([5, 7, 9]);
  });
});

describe("Vector subtraction", () => {
  test("throws an error if the vectors have different lengths", () => {
    const v1 = new Vector(1, 2, 3);
    const v2 = new Vector(4, 5);
    expect(() => v1.subtract(v2)).toThrow("Vectors must have the same length");
  });

  test("throws an error if the argument is not a Vector", () => {
    const v1 = new Vector(1, 2, 3);
    expect(() => v1.subtract(1)).toThrow("Argument must be a Vector");
  });

  test("returns a new Vector object", () => {
    const v1 = new Vector(1, 2, 3);
    const v2 = new Vector(4, 5, 6);
    const v3 = v1.subtract(v2);
    expect(v3).toBeInstanceOf(Vector);
  });

  test("returns a new Vector object with the difference of the vectors", () => {
    const v1 = new Vector(1, 2, 3);
    const v2 = new Vector(4, 5, 6);
    const v3 = v1.subtract(v2);
    expect(v3.vector).toEqual([-3, -3, -3]);
  });
});

describe("Vector dot product", () => {
  test("throws an error if the vectors have different lengths", () => {
    const v1 = new Vector(1, 2, 3);
    const v2 = new Vector(4, 5);
    expect(() => v1.dot(v2)).toThrow("Vectors must have the same length");
  });

  test("throws an error if the argument is not a Vector", () => {
    const v1 = new Vector(1, 2, 3);
    expect(() => v1.dot(1)).toThrow("Argument must be a Vector");
  });

  test("returns a number", () => {
    const v1 = new Vector(1, 2, 3);
    const v2 = new Vector(4, 5, 6);
    const result = v1.dot(v2);
    expect(typeof result).toBe("number");
  });

  test("returns the dot product of the vectors", () => {
    const v1 = new Vector(1, 2, 3);
    const v2 = new Vector(4, 5, 6);
    const result = v1.dot(v2);
    expect(result).toBe(32);
  });
});

describe("Vector cross product", () => {
  test("throws an error if the vectors have different lengths", () => {
    const v1 = new Vector(1, 2, 3);
    const v2 = new Vector(4, 5);
    expect(() => v1.cross(v2)).toThrow("Vectors must have the same length");
  });

  test("throws an error if the vectors have length different than 3", () => {
    const v1 = new Vector(1, 2, 3, 4);
    const v2 = new Vector(5, 6, 7, 8);
    expect(() => v1.cross(v2)).toThrow("Vectors must have a length of 3");
  });

  test("throws an error if the argument is not a Vector", () => {
    const v1 = new Vector(1, 2, 3);
    expect(() => v1.cross(1)).toThrow("Argument must be a Vector");
  });

  test("returns a new Vector object", () => {
    const v1 = new Vector(1, 2, 3);
    const v2 = new Vector(4, 5, 6);
    const v3 = v1.cross(v2);
    expect(v3).toBeInstanceOf(Vector);
  });

  test("returns a new Vector object with the cross product of the vectors", () => {
    const v1 = new Vector(1, 2, 3);
    const v2 = new Vector(4, 5, 6);
    const v3 = v1.cross(v2);
    expect(v3.vector).toEqual([-3, 6, -3]);
  });
});
