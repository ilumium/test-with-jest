class Vector {
  constructor(...args) {
    if (args.length === 0) {
      throw new Error("No arguments provided");
    }
    for (let i = 0; i < args.length; i++) {
      if (typeof args[i] !== "number") {
        throw new Error("All arguments must be numbers");
      }
    }
    this.vector = args;
  }

  add(v) {
    if (!(v instanceof Vector)) {
      throw new Error("Argument must be a Vector");
    }
    if (v.vector.length !== this.vector.length) {
      throw new Error("Vectors must have the same length");
    }
    const result = [];
    for (let i = 0; i < this.vector.length; i++) {
      result.push(this.vector[i] + v.vector[i]);
    }
    return new Vector(...result);
  }

  subtract(v) {
    if (!(v instanceof Vector)) {
      throw new Error("Argument must be a Vector");
    }
    if (v.vector.length !== this.vector.length) {
      throw new Error("Vectors must have the same length");
    }
    const result = [];
    for (let i = 0; i < this.vector.length; i++) {
      result.push(this.vector[i] - v.vector[i]);
    }
    return new Vector(...result);
  }

  dot(v) {
    if (!(v instanceof Vector)) {
      throw new Error("Argument must be a Vector");
    }
    if (v.vector.length !== this.vector.length) {
      throw new Error("Vectors must have the same length");
    }
    let result = 0;
    for (let i = 0; i < this.vector.length; i++) {
      result += this.vector[i] * v.vector[i];
    }
    return result;
  }

  cross(v) {
    if (!(v instanceof Vector)) {
      throw new Error("Argument must be a Vector");
    }
    if (v.vector.length !== this.vector.length) {
      throw new Error("Vectors must have the same length");
    }
    if (this.vector.length !== 3) {
      throw new Error("Vectors must have a length of 3");
    }
    const result = [
      this.vector[1] * v.vector[2] - this.vector[2] * v.vector[1],
      this.vector[2] * v.vector[0] - this.vector[0] * v.vector[2],
      this.vector[0] * v.vector[1] - this.vector[1] * v.vector[0],
    ];
    return new Vector(...result);
  }
}

module.exports = { Vector };
